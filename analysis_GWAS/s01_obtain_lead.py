import numpy as np
import pandas as pd
import scipy.stats as stats

# Select trait
trait = 'height'

# Read files
dfGWAS = pd.read_csv(f'../data/GWAS_{trait}.tsv', sep = '\t', header = None)  # GWAS dataset 
dfIndex = pd.read_csv('../data/snps_index_standard.tsv', sep = '\t')          # standard set of SNPs

# Filter by imputation score, calculate abs zscore and drop duplicates (take max abs zscore) 
dfGWAS.columns = ['SNP', 'allele', 'iscores', 'beta', 'se', 'p'] 
dfGWAS = dfGWAS[dfGWAS['iscores'] > 0.9]                              
dfGWAS['z'] = abs(stats.norm.ppf(dfGWAS['p']/2))                                                            # Calculate abs zscore
dfGWAS['z'] = dfGWAS['z'].replace(np.inf, 39)                                                               # Clip at 39    
dfGWAS = dfGWAS.sort_values(by = 'z', ascending = False)                                                    # Keep max abs zscore
dfGWAS = dfGWAS.drop_duplicates(subset = ['SNP', 'allele'], keep = 'first')

# Save list of lead SNPs for ld calculation (to remove snps in cis with lead snps at rsq 0.05)
dfLead = dfGWAS[dfGWAS['p'] < 5e-8]
dfLead['SNP'].to_csv(f'../analysis_GWAS/{trait}_lead_snps.txt', index = None, header = None)

# Map on standard set of index SNPs
dfGWAS_mapped = dfGWAS[['chr_pos', 'SNP', 'p', 'z']].merge(dfIndex, on = 'SNP', how = 'inner')  
dfGWAS_mapped = dfMapped[['snp_index', 'SNP', 'p', 'z']].rename(columns = {'snp_index': 'snp', 'SNP': 'rsid'})
dfGWAS_mapped.to_csv(f'../analysis_GWAS/GWAS_{trait}_standard.tsv', sep = '\t', index = None)    

