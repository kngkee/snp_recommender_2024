import os
import pickle

import numpy as np
import pandas as pd
import torch

# Select trait
trait = 'height'

# Read mapped GWAS dataset and also read output file from plink (cis snps)
df = pd.read_csv(f'../analysis_GWAS/GWAS_{trait}_standard.tsv', sep = '\t')
dfCis = pd.read_csv(f'../analysis_GWAS/{trait}_ld_calculation.ld', delim_whitespace = True)

# Load embedding
embedding_path = os.path.join('../data', 'emb_standard_molQTL.tsv')
h_item = pd.read_csv(embedding_path, sep='\t').drop('rsid', axis=1)
h_item = torch.tensor(h_item.values)

# Load SNP-to-index mapping
with open(f'../data/mapping_standard.pkl', 'rb') as f:
    mapping = pickle.load(f)           

# Obtain snp indices
df['snp_idx'] = df['snp'].map(mapping)

# Obtain list of lead snps 
snps_lead = list(df[df['p'] < 5e-8]['snp'])
snps_lead_idx = list(df[df['snp'].isin(snps_lead)]['snp_idx'])
snps_cis = list(set(dfCis['SNP_B']))                                                                                     

# Obtain list of eligible tested snps (rsq < 0.05 from all lead SNPs)
dfTested = df[~(df['p'] < 5e-8)]
dfTrans = dfTested[~(dfTested['rsid'].isin(snps_cis))]
snps_trans = list(dfTrans['snp'])
snps_trans_idx = list(dfTrans['snp_idx']) 

# Calculate dot product (similarity) between lead SNP and eligible/trans SNPs, and obtain new mappings
dist = h_item[snps_lead_idx] @ h_item[snps_trans_idx].t()
snp_mapping = dict(zip([i for i in range(len(snps_trans))], snps_trans))
   
# Obtain top 10 recommendations per lead SNP
snps_rec_idx = dist.topk(10)[1].flatten().tolist()
snps_rec = list(map(snp_mapping.get, snps_rec_idx))

# Drop duplicates 
snps_rec = list(dict.fromkeys(snps_rec))      
dfRec = df[df['snp'].isin(snps_rec)]

# Obtain 'unclumped' hits (based on Bonferroni correction)
dfHits = dfRec[dfRec['p'] < (0.05/dfRec.shape[0])] 

# Save file for LD clumping at rsq 0.05 using plink (to obtain number of 'clumped' hits)
dfHits = dfHits[['snp', 'rsid', 'p', 'z']]
dfHits.columns = ['chr_pos', 'SNP', 'P', 'z']
dfHits.to_csv(f'../analysis_GWAS/{trait}_hits_preclumping.assoc', sep = '\t', index = None)