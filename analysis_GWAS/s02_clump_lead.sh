TRAIT='height'

# To obtain list of SNPs at rsq > 0.05 with lead SNPs
plink --bfile ../data/1000Genomes/EUR \
      --r2 \
      --ld-snp-list ${TRAIT}_lead_SNPs.txt \
      --ld-window-kb 1000 \
      --ld-window 99999 \
      --ld-window-r2 0.05 \
      --out ${TRAIT}_ld_calculation
      
# rename files according to trait
rename plink ${TRAIT} * 