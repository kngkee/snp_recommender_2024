TRAIT='height'

# To obtain list of SNPs at rsq > 0.05 with lead SNPs
plink --bfile ../data/1000Genomes/EUR \
      --clump ${TRAIT}_hits_preclumping.assoc \  
      --clump-p1 1 \
      --clump-p2 1 \
      --clump-r2 0.05 \
      --clump-kb 1000  

# rename files according to trait
rename plink ${TRAIT} * 
    
