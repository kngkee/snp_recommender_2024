### Lists of disease categories

# Individual disease categories
diseases_autoimmune = ['Rheumatoid arthritis', 'Systemic lupus erythematosus', 'Psoriasis', 'Psoriatic arthritis', 'Sjögren',  'Systemic sclerosis', 'Graves', 'Autoimmune thyroid disease', 'Multiple sclerosis', 'Inflammatory bowel disease', 'Ulcerative colitis', 'Crohn', 'Asthma', 'Atopic dermatitis', 'Eczema', 'Allergic rhinitis', 'Eosinophilic esophagitis', 'IgA nephropathy', 'Takayasu arteritis', 'Primary biliary cholangitis', 'Alopecia areata', 'Vitiligo']
diseases_cancer = ['Cancer', 'Carcinoma', 'Adenocarcinoma', 'Melanoma', 'Lymphoma', 'Leukaemia', 'Leukemia', 'Tumor', 'Metastasis']
diseases_cardio = ['Coronary artery disease', 'Coronary heart disease', 'Myocardial infarction', 'Hypertension', 'Atrial fibrillation', 'Venous thromboembolism', 'Angina pectoris', 'Ischemic stroke', 'Large artery stroke', 'Idiopathic dilated cardiomyopathy', 'Familial combined hyperlipidemia', 'Aortic valve stenosis']
diseases_neuro = ['Alzheimer', 'Parkinson', 'Dementia with Lewy bodies', 'Migraine', 'Schizophrenia', 'Bipolar disorder', 'Autism spectrum disorder', 'Insomnia', 'Depressive symptoms', 'Depression', 'Post-traumatic stress disorder']
diseases_diabetes = ['Type 1 diabetes', 'Type 2 diabetes', 'Diabetic retinopathy']
diseases_obesity = ['Obesity', 'BMI']
diseases_other = ['COVID-19', 'Pneumonia', 'Hypothyroidism', 'Chronic obstructive pulmonary disease', 'Osteoarthritis', 'Gout', 'Glaucoma', 'Age-related macular degeneration', 'Polycystic ovary syndrome', 'Multisite chronic pain', 'Cirrhosis', 'Temporomandibular joint disorder', 'Colon polyp', 'Uterine fibroids', 'Cholelithiasis', 'Urolithiasis', 'Cholecystitis', 'Kidney stones', 'Hypospadias', 'Dupuytren', 'Nonsyndromic cleft palate', 'Orofacial clefts', 'Cleft lip', 'Inguinal hernia', 'Umbilical hernia', 'Femoral hernia']

# Full list of all disease categories
diseases_all = autoimmune + cancer + cardio + neuro + diabetes + obesity + other

# Full list of all disease categories, excluding individual disease categories
diseases_minus_autoimmune = list(set(diseases_all).difference(autoimmune))
diseases_minus_cancer = list(set(diseases_all).difference(cancer))
diseases_minus_cardio = list(set(diseases_all).difference(cardio))
diseases_minus_neuro = list(set(diseases_all).difference(neuro))
diseases_minus_diabetes = list(set(diseases_all).difference(diabetes))
diseases_minus_obesity = list(set(diseases_all).difference(obesity))
diseases_minus_other = list(set(diseases_all).difference(other))

# List of disease categories, names and titles (to loop through)
disease_categories = [autoimmune, cancer, cardio, neuro, diabetes, obesity, other, diseases_all, diseases_minus_autoimmune, diseases_minus_cancer, diseases_minus_cardio, diseases_minus_neuro, diseases_minus_diabetes, diseases_minus_obesity, diseases_minus_other]                  
disease_category_names = ['autoimmune', 'cancer', 'cardio', 'neuro', 'diabetes', 'obesity', 'other', 'diseases_all', 'diseases_minus_autoimmune', 'diseases_minus_cancer', 'diseases_minus_cardio', 'diseases_minus_neuro', 'diseases_minus_diabetes', 'diseases_minus_obesity', 'diseases_minus_other']                      
disease_category_titles = ['Autoimmune diseases', 'Cancer', 'Cardiovascular diseases', 'Neurological/Psychiatric diseases', 'Diabetes', 'Obesity', 'Other diseases', 'All diseases', 'diseases_minus_autoimmune', 'diseases_minus_cancer', 'diseases_minus_cardio', 'diseases_minus_neuro', 'diseases_minus_diabetes', 'diseases_minus_obesity', 'diseases_minus_other'] 

# Excluded list of traits (non-disease)
traits_excluded = ['Sex hormone-binding globulin levels', 'Nevus count or cutaneous melanoma', 'Epigenetic age acceleration', 'Pulmonary artery enlargement', 'Response to opioid analgesics', 'Tumor necrosis factor', 'Plasma proprotein convertase', 'Agouti-related protein'] 