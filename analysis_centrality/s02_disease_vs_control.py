import numpy as np
import pandas as pd

import scipy.stats as stats
import statsmodels.api as sm

import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns

import disease_lists

# Read files
dfEigs = pd.read_csv('../analysis_centrality/eigs_centrality_index.tsv', sep = '\t', names = ['snp', 'rsid', 'eigs'], header = 0)
dfGWAS = pd.read_csv('../data/GWAS_Catalog_associations.tsv', sep = '\t')
dfSNP = pd.read_csv('../data/GoDMC_meQTL_snps.csv', sep = ',')  

# Obtain list of traits in GWAS Catalog, and number of SNPs associations for each trait
# Save list of all selected traits to manually check for disease traits
trait_counts = dfGWAS['trait'].value_counts()
trait_counts.to_csv('GWAS_Catalog_traits_list.txt', sep = '\t')

# Filter for included disease traits
dfAll = dfGWAS[dfGWAS['trait'].str.contains('|'.join(diseases_all), case = False)].reset_index(drop = True)
dfAll = dfAll[~dfAll['trait'].str.contains('|'.join(traits_excluded), case = False)].reset_index(drop = True)

# Merge to obtain allele frequencies 
dfEigs = dfEigs.merge(dfSNP[['rsid', 'allele_freq']], on = 'rsid', how = 'inner')

# Calculate bins (10 bins, 0.05 width)
bins = [i for i in np.arange(0, 0.55, 0.05)]
labels = [i for i in range(1, 11)] 
dfEigs['freq_bin'] = pd.cut(dfEigs['allele_freq'], bins=bins, labels=labels)
                            
# Obtain summary statistics, boxplot and histogram
for disease_category, disease_category_name, disease_category_title in zip(disease_categories, disease_category_names, disease_category_titles):
    
    # Obtain rows corresponding to disease category and merge to obtain eigs scores
    dfTest = dfAll[dfAll['trait'].str.contains('|'.join(disease_category), case = False)].reset_index(drop = True)
    dfTest = dfTest.merge(dfEigs[['rsid', 'allele_freq', 'freq_bin', 'eigs']], on = 'rsid', how = 'inner')
    dfTest = dfTest[['snp', 'rsid', 'allele_freq', 'freq_bin', 'eigs']].assign(group = 'test').drop_duplicates().reset_index(drop = True)
    n_test = dfTest.shape[0]

    # Obtain list of 'background' SNPs, of which ~10,000 SNPs will be sampled ('control' SNPs)
    snps_tested = list(dfTest['rsid'])
    dfBackground = dfEigs[~dfEigs['rsid'].isin(snps_tested)]

    # Obtain bin proportions and sample ~10000 random rows from 'background' SNPs according to proportions
    bin_fracs = (dfTest['freq_bin'].value_counts()/n_test).to_dict()
    dfControl = pd.concat(dfBackground.sample(round(bin_fracs.get(i)*10000), random_state=1) for i,dfBackground in dfBackground.groupby('freq_bin'))
    dfControl = dfControl[['snp', 'rsid', 'allele_freq', 'freq_bin', 'eigs']].assign(group = 'control')
    n_control = dfControl.shape[0]

    # Concatenate for calculating test statistics and plotting
    dfConcat = pd.concat([dfTest, dfControl], axis = 0).reset_index(drop = True)
            
    # Print test statistics
    print(f'Disease category:', disease_category_name, '\tNumber of disease SNPs:', n_test, '  \tNumber of control SNPs:', n_control)
    t_statistic, t_pvalue = stats.ttest_ind(dfTest['eigs'], dfControl['eigs'], equal_var=False, alternative='two-sided')
    u_statistic, u_pvalue = stats.mannwhitneyu(dfTest['eigs'], dfControl['eigs'], alternative='two-sided') 
    print('t_test:', t_statistic, '\t', t_pvalue, '\t\tmw_test:', u_statistic, '\t',  u_pvalue)
    print()

    # Violinplots  
    x1, x2, y, h, z = 0, 1, 0.0054, 0.0001, 0.0005
    matplotlib.rcParams.update({'font.size': 15})
    box_plot = sns.violinplot(x = 'group', y = 'eigs', data = dfConcat)
    plt.plot([x1, x1, x2, x2], [y, y+h, y+h, y], linewidth=1, color='k')
    plt.text((x1+x2)*.5, y+h, f"p = {u_pvalue:.2}", ha='center', va='bottom', color='k')
    plt.text(0, z, f"N = {n_test}", ha='center', va='top', color='k')
    plt.text(1, z, f"N = {n_control}", ha='center', va='top', color='k')
    plt.title(disease_category_title, fontweight="bold")  
    plt.xticks([0, 1], ['Disease', 'Control'])
    plt.xlabel('Group')
    plt.ylabel('Eigenvector Centrality')
    plt.ylim(0.0000, 0.0060) 
    plt.tight_layout()
    plt.savefig(f'../analysis_centrality/violinplot_{disease_category_name}.png') 
    plt.close()
 