import os
import pickle

import numpy as np
import pandas as pd
import torch

import graph_tool.all as gt

# Read files
df = pd.read_csv('../data/snps_index_indep.tsv', sep = '\t')

# Load embedding
embedding_path = os.path.join('../data', 'emb_pseudo-independent_molQTL.tsv')
h_item = pd.read_csv(embedding_path, sep='\t').drop('rsid', axis=1)
h_item = torch.tensor(h_item.values)

# Calculate similarity matrix and take absolute value
dist = h_item @ h_item.t() 
dist = torch.abs(dist)    

# Create undirected graph, obtaining edges from adjacency matrix (upper triangular)
adj = np.triu(dist,1)
edge_idx = adj.nonzero()
dot_products = adj[edge_idx]

g = gt.Graph(directed=False)
g.add_edge_list(np.transpose(edge_idx))

# Add edge weights as an edge PropertyMap
edge_weights = g.new_edge_property("double")
edge_weights.a = dot_products 
g.edge_properties['edge_weight'] = edge_weights

# Calculate eigenvector centrality
lead, eigs = gt.eigenvector(g, weight=g.edge_properties['edge_weight'], vprop=None, epsilon=1e-04, max_iter=100)

# Add values to dataframe and save
df['eigs'] = list(eigs)
df.to_csv('../analysis_centrality/eigs_centrality_index.tsv', sep = '\t', index = None)