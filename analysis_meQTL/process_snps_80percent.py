### NOTE:
### The code below was adapted from the file named 'process_movielens1m.py' (available at the following GitHub repository: https://github.com/dmlc/dgl/tree/master/examples/pytorch/pinsage)

# Load modules
import argparse
import os
import pickle

import pandas as pd
import scipy.sparse as ssp
from builder import PandasGraphBuilder
from data_utils import *

import dgl


# Choose between 'standard' and 'indep' set of SNPs
snps = 'standard'
# snps = 'indep'


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", type=str)
    parser.add_argument("out_directory", type=str)
    args = parser.parse_args()
    directory = args.directory
    out_directory = args.out_directory
    os.makedirs(out_directory, exist_ok=True)                 
    
    # Load data   
    assoc = pd.read_csv(f'../data/meQTL_{snps}.tsv', sep = '\t', header = 0, names = ['snp', 'trait', 'z']) 
    trait = assoc[['trait']].drop_duplicates()
    snp = assoc[['snp']].drop_duplicates()
    
    # Build graph
    graph_builder = PandasGraphBuilder()
    graph_builder.add_entities(trait, "trait", "trait")
    graph_builder.add_entities(snp, "snp", "snp")
    graph_builder.add_binary_relations(
        assoc, "trait", "snp", "affected_by"
    )
    graph_builder.add_binary_relations(
        assoc, "snp", "trait", "affects"
    )

    g = graph_builder.build()
    
    n_edges = g.num_edges("affected_by")
    
    # Add edge features ('zscore')
    g.edges["affected_by"].data["zscore"] = torch.LongTensor(
        assoc["z"].values
    )
    g.edges["affects"].data["zscore"] = torch.LongTensor(
        assoc["z"].values
    )
    
    # Train-validation-test split
    test_indices = assoc.sample(frac=0.1, random_state=1).index
    val_indices = assoc.sample(frac=0.1, random_state=2).index
    val_indices = val_indices.difference(test_indices)
    train_indices = assoc[~(assoc.index.isin(test_indices) | assoc.index.isin(val_indices))].index
            
    assoc['train'] = assoc.index.isin(train_indices)
    assoc['valid'] = assoc.index.isin(val_indices)
    assoc['test'] = assoc.index.isin(test_indices)

    # Build the graph with training interactions only.
    train_g = build_train_graph(
        g, train_indices, "trait", "snp", "affected_by", "affects"
    )

    # Build the user-item sparse matrix for validation and test set.
    val_matrix, test_matrix = build_val_test_matrix(
        g, val_indices, test_indices, "trait", "snp", "affected_by"
    )
    
    # Dump the graph and the datasets

    dgl.save_graphs(os.path.join(out_directory, "train_g.bin"), train_g)

    dataset = {
        "val-matrix": val_matrix,
        "test-matrix": test_matrix,
        "item-texts": None,             
        "item-images": None,
        "user-type": "trait",
        "item-type": "snp",
        "user-to-item-type": "affected_by",
        "item-to-user-type": "affects",         
        "zscore-edge-column": "zscore",
    }

    with open(os.path.join(out_directory, "data.pkl"), "wb") as f:
        pickle.dump(dataset, f)