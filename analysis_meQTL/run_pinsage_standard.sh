# Choose dataset
QTL='molQTL'                    # Or change to 'meQTL'
TRAIN_FRAC='100percent'         # Or change to '80percent'

# Process data for running PinSage
# Note: Set snps = 'standard' and also select between 'molQTL' and 'meQTL' in the following file: process_snps_${TRAIN_FRAC}.py
python process_snps_${TRAIN_FRAC}.py . ./data_processed_${QTL}_standard_${TRAIN_FRAC}   
               
# Run PinSage
python model.py ./data_processed_${QTL}_standard_${TRAIN_FRAC} \
    --num-random-walks 10 \
    --num-neighbors 20 \
    --num-layers 2 \
    --hidden-dims 512 \
    --batch-size 512 \
    --device cuda:0 \
    --num-epochs 300 \
    --batches-per-epoch 5000 \
    --num-workers 1 \
    --eval-hitrate 0 \
    --save-embedding 1 \
    --lr 5e-5 \
    -k 100
