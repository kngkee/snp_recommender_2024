# Choose dataset
QTL='molQTL'                    # Or change to 'meQTL'
TRAIN_FRAC='100percent'         # Or change to '80percent'

# Process data for running PinSage 
# Note: Set snps = 'indep' and also select between 'molQTL' and 'meQTL' in the following file: process_snps_${TRAIN_FRAC}.py
python process_snps_${TRAIN_FRAC}.py . ./data_processed_${QTL}_indep_${TRAIN_FRAC}                  
               
# Run PinSage
python model.py ./data_processed_${QTL}_indep_${TRAIN_FRAC} \
    --num-random-walks 10 \
    --num-neighbors 20 \
    --num-layers 2 \
    --hidden-dims 256 \
    --batch-size 256 \
    --device cuda:0 \
    --num-epochs 200 \
    --batches-per-epoch 5000 \
    --num-workers 1 \
    --eval-hitrate 1 \
    --save-embedding 1 \
    --lr 5e-5 \
    -k 100 

