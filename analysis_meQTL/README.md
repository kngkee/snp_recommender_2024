
The following files can be downloaded from the original GitHub repository: https://github.com/dmlc/dgl/tree/master/examples/pytorch/pinsage
- builder.py
- data_utils.py
- layers.py
- sampler.py

<br />

The following files have been adapted from the original files available at the above GitHub repository.
- evaluation.py
- model.py
- process_snps_80percent.py (adapted from process_movielens1m.py)
- process_snps_100percent.py (adapted from process_movielens1m.py)











