import numpy as np
import pandas as pd

from ast import literal_eval

# Choose between standard and pseudo-independent set of SNPs
snps = 'standard'
# snps = 'indep'

# Read file
df = pd.read_csv(f'../analysis_eQTL/eQTL_recommendations_evaluation_{snps}.tsv', sep = '\t')

# Ensure correct data type
df['snp_assoc'] = df['snp_assoc'].apply(literal_eval)

# Obtain number of total number of genes and snp associations
n_genes = df.shape[0]                                                                      
n_assoc = df['snp_assoc'].str.len().sum() 

# Obtain median (IQR) number of snp associations
assoc = df['snp_assoc'].str.len()
print("Mean number of SNP associations:", assoc.mean())
print("Median number of SNP associations:", assoc.median())
print("IQR number of SNP associations:", np.percentile(assoc, [25 ,75]))

# Select k: Number of recommendations
k = 1
k = 10
k = 100
k = 1000

# Convert to list of strings for subsetting
K = list(np.arange(k))
K = [str(i) for i in K]
dfTest = df[K]

# Binary hit rate (Use k = 1, 10 or 100)
total = 0    
for i in range(n_genes):
    if dfTest.iloc[i].isin(df.iloc[i, 2]).any() == True:
        total += 1
print("Number of hits:", total)
print("Hit rate:", total/n_genes)                                                                                         

# Adjusted hit rate (Use k = 1000)
total_hits = 0
total_targets = 0    
for i in range(n_genes):
    row_targets = len(df.iloc[i, 2])
    subtotal_hits = dfTest.iloc[i, 0:row_targets].isin(df.iloc[i, 2]).sum() 
    total_hits += subtotal_hits
    total_targets += row_targets
print("Number of hits:", total_hits)                   
print("Number of possible targets:",total_targets) 
print("Adjusted hit rate:", total_hits/total_targets)