import numpy as np
import pandas as pd

# Choose between standard and pseudo-independent set of SNPs
snps = 'standard'
# snps = 'indep'

# Read files
dfeQTL_sig = pd.read_csv('../data/eQTL_significant.tsv', sep = '\t')
dfeQTL_all = pd.read_csv('../data/eQTL_all.tsv', sep = '\t')
dfIndex = pd.read_csv(f'../data/snps_index_{snps}.tsv', sep = '\t')
             
# Map on index SNPs 
dfMapped_sig = dfeQTL_sig[['Gene', 'snp', 'z']].merge(dfIndex['snp_index'], left_on = 'snp', right_on = 'snp_index', how = 'inner')  
dfMapped_all = dfeQTL_all[['Gene', 'snp']].merge(dfIndex['snp_index'], left_on = 'snp', right_on = 'snp_index', how = 'inner')  

# Obtain lead SNP (max abs zscore) for each gene
dfLead = dfMapped_sig.sort_values(by = 'z', ascending = False) 
dfLead = dfLead.drop_duplicates(subset = ['Gene'], keep = 'first').reset_index(drop = True).drop('z', axis = 1)

# Obtain list of associated SNPs for each gene
dfAssoc = dfMapped_sig[['Gene', 'snp_index']].drop_duplicates().reset_index(drop = True)                           
dfAssoc = pd.merge(dfAssoc, dfLead, on = ['Gene', 'snp_index'], how = "outer", indicator=True)      # drop lead snp from associations
dfAssoc = dfAssoc[dfAssoc['_merge'] == 'left_only']
dfAssoc = dfAssoc.drop('_merge', axis = 1).rename(columns = {'snp_index': 'snp_assoc'})
dfAssoc = dfAssoc.groupby('Gene')['snp_assoc'].apply(list)
  
# Obtain list of tested SNPs for each gene 
dfTested = dfMapped_all.groupby('Gene')['snp_index'].apply(list).reset_index()
dfTested = dfTested.rename(columns = {'snp_index': 'snp_tested'})

# Merge dataframes to obtain lead SNP, list of associated SNPs & list of tested SNPs for each gene
dfTemp = dfLead.merge(dfAssoc, left_on = 'Gene', right_index = True, how = 'inner', validate = 'one_to_one') 
dfTemp = dfTemp.rename(columns = {'snp_index': 'snp_lead'})
dfMerged = dfTemp.merge(dfTested, on = 'Gene', how = 'inner')
dfMerged.to_csv(f'../analysis_eQTL/eQTL_evaluation_{snps}_intermediate.tsv', sep = '\t', index = None)