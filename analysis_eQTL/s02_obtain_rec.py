import os
import pickle

import numpy as np
import pandas as pd
import torch

from ast import literal_eval

# Choose between standard and pseudo-independent set of SNPs (K: number of recommendations)
snps = 'standard'
embedding = 'emb_standard_meQTL.tsv'
K = 1000

# snps = 'indep'
# embedding = 'emb_pseudo-independent_meQTL.tsv'
# K = 100

# Read file
df = pd.read_csv(f'../analysis_eQTL/eQTL_evaluation_{snps}_intermediate.tsv', sep = '\t')

# Load embedding
embedding_path = os.path.join(f'../data', embedding)
h_item = pd.read_csv(embedding_path, sep='\t').drop('rsid', axis=1)
h_item = torch.tensor(h_item.values)

# Load SNP-to-index mapping
with open(f'../data/mapping_{snps}.pkl', 'rb') as f:
    mapping = pickle.load(f)

# Ensure correct data type 
df['snp_assoc'] = df['snp_assoc'].apply(literal_eval)
df['snp_tested'] = df['snp_tested'].apply(literal_eval)

# Create column containing snp indices
df['snp_lead_idx'] = df['snp_lead'].map(mapping)
df['snp_tested_idx'] = None
for i in range(df.shape[0]):
    df.at[i, 'snp_tested_idx'] = list(map(mapping.get, df.loc[i, 'snp_tested']))

# Obtain recommendations for each gene 
df['Recommendations'] = None
df['Recommendations_idx'] = None
for i in range(df.shape[0]):

    # Calculate dot product (similarity) between lead SNP and all tested SNPs, and obtain new mapping
    dist = h_item[df.loc[i, 'snp_lead_idx']] @ h_item[df.loc[i, 'snp_tested_idx']].t()
    tested = df.loc[i, 'snp_tested']
    rev_mapping = dict(zip([i for i in range(len(tested))], tested))
    
    # Set distance to lead SNP as -np.inf to avoid recommending lead SNP 
    lead = df.loc[i, 'snp_lead']                                
    lead_idx = list(rev_mapping.values()).index(lead)
    dist[lead_idx] = -np.inf
    
    # Obtain snp idx for recommendations
    df.at[i, 'Recommendations_idx'] = dist.topk(K)[1].tolist()
    df.at[i, 'Recommendations'] = list(map(rev_mapping.get, df.loc[i, 'Recommendations_idx']))

# Merge to obtain combined dataframe for evaluation
dfRec = pd.DataFrame(df['Recommendations'].values.tolist())                              
dfEval = df[['Gene', 'snp_lead', 'snp_assoc']].merge(dfRec, left_index = True, right_index = True, how = 'inner') 
dfEval.to_csv(f'../analysis_eQTL/eQTL_recommendations_evaluation_{snps}.tsv', sep = '\t', index = None)