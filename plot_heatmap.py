
########## Rearrange embedding #########
########################################

import os

import numpy as np
import pandas as pd

import pickle
import torch

# Load embedding
embedding_path = os.path.join('../data/', 'emb_standard_molQTL.tsv')
emb_unordered = pd.read_csv(embedding_path, sep='\t').drop('rsid', axis=1)
emb_unordered = torch.tensor(h_item.values)

# Load SNP-to-index mapping
with open(f'../data/mapping_indep.pkl', 'rb') as f:
    mapping = pickle.load(f)

# Reorder SNPs by chr and pos
df = pd.DataFrame(list(mapping)).rename(columns = {0: 'snp'})
df[['chr', 'pos']] = df['snp'].str.split(':', expand=True)
df[['chr', 'pos']] = df[['chr', 'pos']].astype(int)

df = df.sort_values(by = ['chr', 'pos'])
snp_ordering = list(df.index)
emb = emb_unordered[snp_ordering]

# Save ordered SNPs and ordered embedding
df.to_csv('../data/snps_ordered.tsv', sep='\t')

with open('../data/embedding_indep_ordered.pkl', 'wb') as f:
    pickle.dump(emb, f)


####### Plot similarity matrix heatmap #########
################################################

# Load modules
import os
import pickle

import math
import numpy as np
import pandas as pd
import scipy.stats as stats

import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns

import torch

# Define some useful functions
def roundup(x):
    return math.ceil(x / 1e7) 

def get_closest(lst, target, flank, min_pos, max_pos):
    if flank == 'left':
        lst = [pos for pos in lst if pos < target]
        if lst == []:
            closest_number = min_pos
        else:    
            closest_number = min(lst, key=lambda x: abs(x-target))
    elif flank == 'right':
        lst = [pos for pos in lst if pos > target]
        if lst == []:
            closest_number = max_pos
        else:
            closest_number = min(lst, key=lambda x: abs(x-target))
    return closest_number
    
def get_xpos(dfChr, pos):
    xpos = dfChr[dfChr['pos'] == pos_left].index.item()
    return xpos
    
def get_xpos_interpolate(dfChr, pos, pos_left, pos_right):
    xpos_left, xpos_right = get_xpos(dfChr, pos_left), get_xpos(dfChr, pos_right)  
    xpos_diff = xpos_right - xpos_left
    frac_shift = (pos - pos_left) / (pos_right - pos_left)
    xpos = xpos_left + (frac_shift * xpos_diff)
    return xpos, xpos_left, xpos_right

# Load data
df = pd.read_csv('../data/snps_ordered.tsv', sep='\t')

with open(f'../data/embedding_indep_ordered.pkl', 'rb') as f:
    emb = pickle.load(f)

# Loop through chromosomes
for chr in range(1, 23):
    
    # Subset embedding for specific chromosome
    dfChr = df[df['chr'] == chr]
    indices = list(dfChr.index)
    
    embChr = emb[indices]
    distChr = embChr @ embChr.t() 
    
    dfChr = dfChr.reset_index(drop=True)
    min_pos, max_pos = dfChr['pos'].min(), dfChr['pos'].max()
     
    # Obtain genomic coordinates and corresponding 'x' positions
    xticks_pos = [np.nan]                                                                   # Initialised just to avoid index error later on 
    xticks_labels = [0]
    
    for i in range(roundup(max_pos) + 1):
        pos = i*1e7
        pos_left = get_closest(list(dfChr['pos']), pos, 'left', min_pos, max_pos)
        pos_right = get_closest(list(dfChr['pos']), pos, 'right', min_pos, max_pos)       
        
        xpos, xpos_left, xpos_right = get_xpos_interpolate(dfChr, pos, pos_left, pos_right)
        
        if abs(xpos - xticks_pos[-1]) < 0.06 * len(indices):                                # Skip tickmark to avoid overlapping labels on graph
            continue
        
        xticks_pos.append(xpos)
        xticks_labels.append(int(pos / 1e6))                                                # Divide by 1e6 to get label in Mb
        
    # Obtain ticks and labels    
    xticks_indices = np.where(~np.isnan(xticks_pos))[0]
    xticks_pos = [i for i in xticks_pos if ~np.isnan(i)]
    xticks_labels = np.array(xticks_labels)
    xticks_labels = xticks_labels[xticks_indices]
    
    # Plot heatmap
    matplotlib.rcParams.update({'font.size': 15})
    fig, ax = plt.subplots()

    ax.set_xticks(xticks_pos)
    ax.set_xticklabels(xticks_labels, fontsize=8)
    ax.set_yticks(xticks_pos)
    ax.set_yticklabels(xticks_labels, fontsize=8)
    ax.xaxis.tick_top()
    ax.xaxis.set_label_position('top') 

    ax.set_title(f'Chromosome {chr}', y=-0.01, pad=-14)
    ax.set_xlabel('Position (Mb)')
    ax.set_ylabel('Position (Mb)')

    ax.imshow(distChr, cmap='Reds', vmin=-1, vmax=10)
    heatmap=ax.get_children()[9]
    plt.colorbar(heatmap, ax=ax)

    plt.savefig(f'../data/dist_matrix_heatmap_chr{chr}.png')       
    plt.close()
