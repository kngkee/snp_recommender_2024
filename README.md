# SNP recommender system

This repository contains the code associated with the following paper: [Leveraging molecular-QTL co-association to predict novel disease-associated genetic loci using a graph convolutional neural network (Ng-Kee-Kwong & Bretherick, 2024)](https://www.medrxiv.org/content/10.1101/2024.03.04.24303678v1).

## Installation and setup

Clone this repository on your local machine

`git clone https://git.ecdf.ed.ac.uk/kngkee/snp_recommender_2024.git`

Create virtual environment and install dependencies (use Python 3.7.3)

`pip install -r requirements.txt`


## Repository organisation

- `analysis_meQTL`: Directory containing code for the "**Model 1: Predicting SNP-CpG associations**" section

- `analysis_eQTL`: Directory containing code for the "**Model 2: Predicting SNP-RNA associations**" section  

- `analysis_centrality`: Directory containing code for the "**Model 3: Node importance linked to disease association**" section 

- `analysis_GWAS`: Directory containing code for the "**Augmentation of Existing GWAS**" section

- `plot_heatmap.py`: Code for plotting similarity heatmaps

- `data`: Directory containing the datasets associated with this project (See [Ng-Kee-Kwong & Bretherick, 2024](https://www.medrxiv.org/content/10.1101/2024.03.04.24303678v1)). The ‘standard’ and ‘pseudo-independent’ embeddings (meQTL only, and meQTL
plus eQTL) – from which SNP similarity can be computed – are available at:
[https://doi.org/10.7488/ds/7689].



